#!/bin/bash

. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGES=(glibc gcc diffutils glibc-langpack-tr glibc-gconv-extra)

rlJournalStart
    rlPhaseStartSetup
        for p in "${PACKAGES[@]}"; do
            rlAssertRpm "$p"
        done; unset p
    rlPhaseEnd

    rlPhaseStartTest
        rlRun " [ $(systemctl | grep systemd | wc -l) -gt 100 ]" 0 "OK"
    rlPhaseEnd

rlJournalPrintText
rlJournalEnd
